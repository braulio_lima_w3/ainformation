
# Generic medication:

- freecodecamp [LINK](http://freecodecamp.org)
    - textual processing via python

# AI study shizzle:

- Medium AI skills worth less (maybe...) [LINK](https://medium.com/@szopa/your-ai-skills-are-worth-less-than-you-think-e4b5640adb4f?source=email-c66eac111639-1549966626322-digest.reader------0-50------------------2599453e_077d_44d4_94f7_951790a1b86a-1&sectionName=top)
- Neural Network Tutorial [LINK](https://www.edureka.co/blog/neural-network-tutorial/)
- Deep Learning Book [LINK](http://deeplearningbook.com.br/capitulos/)

# Articles:

- Understanding LSTM [LINK](http://colah.github.io/posts/2015-08-Understanding-LSTMs/)
- Sequence Classification with LSTM Recurrent Neural Networks in Python with Keras [LINK](https://machinelearningmastery.com/sequence-classification-lstm-recurrent-neural-networks-python-keras/)

# Papers:

- Dialog Intent Classification w/ Long Short-Term Memory Networks [LINK](http://tcci.ccf.org.cn/conference/2017/papers/1158.pdf)
- Long Short-Term Memory [LINK](https://www.bioinf.jku.at/publications/older/2604.pdf)

# Courses:

- Guides:
    - ($+) to ($$$$$), (?)
        - $ the amount of money for certificate
            - \+ or more
        - ? content is free
        - \- no cert
    - (⏲) Timed
        - (⏲:DATE) - Timed, starts at...
        - ? or not
    - (Xhrs) Time spent learning
        - \+ or more
    - (en-US) Language
        - multi
    - (!) Might not be relevant
    - (Tip:) or (Info:) tip

Topics:

- Artificial Intelligence (ColumbiaX) ($$$$?) (⏲:03-02-2019) (en) [LINK](https://www.edx.org/course/artificial-intelligence-ai)
- Brilliant.org (Info: Problem solving for math) ($$$+) (en) (Tip: Youtubers tend to give out 20% discounts) [LINK](http://brilliant.org)
- Data Science (HavardX) ($$?) (⏲:12-10-2018) (en) [LINK](https://www.edx.org/course/data-science-machine-learning)
- Deep Learning (Cognitive Class) (en) (20hr+) [LINK](https://cognitiveclass.ai/learn/deep-learning/)
- Deep Learning With Tensorflow (Cognitive Class) (en) [LINK](https://cognitiveclass.ai/courses/deep-learning-tensorflow/)
- Deep Learning with Tensorflow (IBM) ($$$?) (en) [LINK](https://www.edx.org/course/deep-learning-with-tensorflow)
- Intro to Artificial Intelligence (UDACITY) ($$$$$?) (en) [LINK](https://www.udacity.com/course/intro-to-artificial-intelligence--cs271)
- Intro to Machine Learning (UDACITY) ($$$$$?) (en) [LINK](https://www.udacity.com/course/intro-to-machine-learning--ud120)
- Khan Academy (Info: Math) (Khan Academy) (Info:pre-k to 12+ math) (-) (?) (multi) [LINK](http://khanacademy.org)
- Machine Learning (ColumbiaX) ($$$$?) (⏲:04-02-2019) (en) [LINK](https://www.edx.org/course/machine-learning)
- Machine Learning (Stanford) ($$+?) (en) [LINK](https://www.coursera.org/learn/machine-learning)
- Machine Learning Crash Course (Google) (-) (en) [LINK](https://developers.google.com/machine-learning/crash-course/)
- Mathraining (Info: Math) (-) (?) (fr) [LINK](http://www.mathraining.be)

## Course Aggregation

- OSSU (Generic)  (- to $$$$+ ?) (⏲?) (1000hrs+) (en) [LINK](https://github.com/ossu/computer-science)
- For more, check out:
    - [edx](http://edx.org) (multi) ($+?) (⏲?) (1hr+)
    - [Coursera](http://coursera.org) (multi) ($+?) (⏲?) (1hr+)
    - [MIT OCW](https://ocw.mit.edu/index.htm) (-)
    - [Open Yale Courses](https://oyc.yale.edu/courses) (!)
    - [Class Central](https://www.class-central.com/) (Tip:MOOC Aggregator/Search Engine) (multi)
    - [Cognitive Class](https://cognitiveclass.ai/courses/) (en)

# Books:

- Hands-On Machine Learning with Scikit-Learn and TensorFlow (O'Reilly) (2017)

# Algorithms:

- Graphs [LINK](https://www.geeksforgeeks.org/graph-data-structure-and-algorithms/)
- A* algorithm [LINK](https://www.redblobgames.com/pathfinding/a-star/introduction.html)